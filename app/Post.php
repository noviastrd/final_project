<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = "postingan";
    protected $fillable = ["isi_postingan", "user_id"];


    function upfoto(){
    	return $this->hasMany('App\Foto');
    }

    public function author(){
    	return $this->belongsTo('App\User','user_id');
    }
}

