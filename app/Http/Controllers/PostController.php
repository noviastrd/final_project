<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Post;
use Auth;

class PostController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $postingan = DB::table('postingan')->join('user', 'user_id', '=', 'user.id')
        ->select(
            'postingan.id',
            'postingan.isi_postingan',
            'postingan.created_at',
            'postingan.updated_at',
            'postingan.user_id',
            'user.name'
        )
        ->get();

        // $postingan = Post::all();

        return view('post.feeds', compact('postingan'));    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('post.posting');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'isi_postingan' => 'required'
            // 'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);

        //build
        // $query = DB::table('postingan')->insert([
        //     "isi_postingan" => $request["isi_postingan"]
        // ]);

        //ORM
        // $pertanyaan = new Pertanyaan;
        // $pertanyaan->judul = $request['judul'];
        // $pertanyaan->isi = $request['isi'];
        // $pertanyaan->save();

        $post = Post::create([
            
            "isi_postingan" => $request["isi_postingan"],
            "user_id" => Auth::id()
        ]);
        return redirect('/post')->with('success','Postingan Berhasil Ditambah');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $postingan = Post::find($id);

        return view('komentar.komentar', compact('postingan'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $postingan = Post::find($id);        
        return view('post.edit_post', compact('postingan'));
        //return view('post.edit_post');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $update = Post::where("id", $id) -> update ([
            "isi_postingan" => $request ["isi_postingan"]
        ]);


        return redirect('/post')->with('success','Postingan Berhasil Diupdate');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Post::destroy($id);
        return redirect('/post')->with('success','Postingan Berhasil Dihapus');
    }
}
