<!DOCTYPE html>
<html lang="en">
<head>

	<title>Landing Page</title>

	<!-- Required meta tags always come first -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="x-ua-compatible" content="ie=edge">

	<!-- Main Font -->
	<script src="{{ asset('/olympus-html/js/webfontloader.min.js')}}"></script>

	<script>
		WebFont.load({
			google: {
				families: ['Roboto:300,400,500,700:latin']
			}
		});
	</script>

	<!-- Bootstrap CSS -->
	<link rel="stylesheet" type="text/css" href="{{ asset('/olympus-html/Bootstrap/dist/css/bootstrap-reboot.css')}}">
	<link rel="stylesheet" type="text/css" href="{{ asset('/olympus-html/Bootstrap/dist/css/bootstrap.css')}}">
	<link rel="stylesheet" type="text/css" href="{{ asset('/olympus-html/Bootstrap/dist/css/bootstrap-grid.css')}}">

	<!-- Main Styles CSS -->
	<link rel="stylesheet" type="text/css" href="{{ asset('/olympus-html/css/main.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{ asset('/olympus-html/css/fonts.min.css')}}">



</head>

<body class="landing-page">

<div class="content-bg-wrap"></div>


<!-- Header Standard Landing  -->

<div class="header--standard header--standard-landing" id="header--standard">
	<div class="container">
		<div class="header--standard-wrap">

			<a href="#" class="logo">
				<div class="img-wrap">
					<img src="{{ asset('/olympus-html/img/logo.png')}}" alt="Olympus">
					<img src="{{ asset('/olympus-html/img/logo-colored-small.png')}}" alt="Olympus" class="logo-colored">
				</div>
				<div class="title-block">
					<h6 class="logo-title">FindFriends</h6>
					<div class="sub-title">SOCIAL NETWORK</div>
				</div>
			</a>

			<a href="#" class="open-responsive-menu js-open-responsive-menu">
				<svg class="olymp-menu-icon"><use xlink:href="{{ asset('/olympus-html/svg-icons/sprites/icons.svg#olymp-menu-icon')}}"></use></svg>
			</a>

			<div class="nav nav-pills nav1 header-menu">
				<div class="mCustomScrollbar">
					<ul>
						<li class="nav-item">
							<a href="#" class="nav-link">Home</a>
						</li>
						<li class="nav-item dropdown">
							<a class="nav-link dropdown-toggle" data-hover="dropdown" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false" tabindex='1'>Profile</a>
							<div class="dropdown-menu">
								<a class="dropdown-item" href="#">Profile Page</a>
								<a class="dropdown-item" href="#">Newsfeed</a>
								<a class="dropdown-item" href="#">Post Versions</a>
							</div>
						</li>
						<li class="nav-item dropdown dropdown-has-megamenu">
							<a href="#" class="nav-link dropdown-toggle" data-hover="dropdown" data-toggle="dropdown" role="button" aria-haspopup="false" aria-expanded="false" tabindex='1'>Forums</a>
							<div class="dropdown-menu megamenu">
								<div class="row">
									<div class="col col-sm-3">
										<h6 class="column-tittle">Main Links</h6>
										<a class="dropdown-item" href="#">Profile Page<span class="tag-label bg-blue-light">new</span></a>
										<a class="dropdown-item" href="#">Profile Page</a>
										<a class="dropdown-item" href="#">Profile Page</a>
										<a class="dropdown-item" href="#">Profile Page</a>
										<a class="dropdown-item" href="#">Profile Page</a>
										<a class="dropdown-item" href="#">Profile Page</a>
									</div>
									<div class="col col-sm-3">
										<h6 class="column-tittle">BuddyPress</h6>
										<a class="dropdown-item" href="#">Profile Page</a>
										<a class="dropdown-item" href="#">Profile Page</a>
										<a class="dropdown-item" href="#">Profile Page<span class="tag-label bg-primary">HOT!</span></a>
										<a class="dropdown-item" href="#">Profile Page</a>
										<a class="dropdown-item" href="#">Profile Page</a>
										<a class="dropdown-item" href="#">Profile Page</a>
									</div>
									<div class="col col-sm-3">
										<h6 class="column-tittle">Corporate</h6>
										<a class="dropdown-item" href="#">Profile Page</a>
										<a class="dropdown-item" href="#">Profile Page</a>
										<a class="dropdown-item" href="#">Profile Page</a>
										<a class="dropdown-item" href="#">Profile Page</a>
										<a class="dropdown-item" href="#">Profile Page</a>
										<a class="dropdown-item" href="#">Profile Page</a>
									</div>
									<div class="col col-sm-3">
										<h6 class="column-tittle">Forums</h6>
										<a class="dropdown-item" href="#">Profile Page</a>
										<a class="dropdown-item" href="#">Profile Page</a>
										<a class="dropdown-item" href="#">Profile Page</a>
										<a class="dropdown-item" href="#">Profile Page</a>
										<a class="dropdown-item" href="#">Profile Page</a>
										<a class="dropdown-item" href="#">Profile Page</a>
									</div>
								</div>
							</div>
						</li>
						<li class="nav-item">
							<a href="#" class="nav-link">Terms & Conditions</a>
						</li>
						<li class="nav-item">
							<a href="#" class="nav-link">Events</a>
						</li>
						<li class="nav-item">
							<a href="#" class="nav-link">Privacy Policy</a>
						</li>
						<li class="close-responsive-menu js-close-responsive-menu">
							<svg class="olymp-close-icon"><use xlink:href="{{ asset('/olympus-html/svg-icons/sprites/icons.svg#olymp-close-icon')}}"></use></svg>
						</li>
						<li class="nav-item js-expanded-menu">
							<a href="#" class="nav-link">
								<svg class="olymp-menu-icon"><use xlink:href="{{ asset('/olympus-html/svg-icons/sprites/icons.svg#olymp-menu-icon')}}"></use></svg>
								<svg class="olymp-close-icon"><use xlink:href="{{ asset('/olympus-html/svg-icons/sprites/icons.svg#olymp-close-icon')}}"></use></svg>
							</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- ... end Header Standard Landing  -->
<div class="header-spacer--standard"></div>

<div class="container">
	<div class="row display-flex">
		<div class="col col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
			<div class="landing-content">
				<h1>Sealamat datang di Web FindFriends</h1>
				<p>Pada web kami kalian dapat mencari sebanyak-banyaknya teman
				</p>
				<a href="#" class="btn btn-md btn-border c-white">Register Now!</a>
			</div>
		</div>

		<div class="col col-xl-5 col-lg-6 col-md-12 col-sm-12 col-12">
			
			<!-- Login-Registration Form  -->

			@if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif
			
			<div class="registration-login-form">
				<!-- Nav tabs -->
				<ul class="nav nav-tabs" role="tablist">
					<li class="nav-item">
						<a class="nav-link active" data-toggle="tab" href="#home" role="tab">
							<svg class="olymp-login-icon"><use xlink:href="{{ asset('/olympus-html/svg-icons/sprites/icons.svg#olymp-login-icon')}}"></use></svg>
						</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" data-toggle="tab" href="#profile" role="tab">
							<svg class="olymp-register-icon"><use xlink:href="{{ asset('/olympus-html/svg-icons/sprites/icons.svg#olymp-register-icon')}}"></use></svg>
						</a>
					</li>
				</ul>
			
				<!-- Tab panes -->
				<div class="tab-content">
					<div class="tab-pane active" id="home" role="tabpanel" data-mh="log-tab">
						<div class="title h6">Registrasi</div>
						<form class="content">
							<div class="row">
								<div class="col col-12 col-xl-12 col-lg-12 col-md-12 col-sm-12">
									<div class="form-group label-floating is-empty">
										<label class="control-label">Nama Lengkap</label>
										<input class="form-control" placeholder="" type="text">
									</div>
								</div>
								<div class="col col-12 col-xl-12 col-lg-12 col-md-12 col-sm-12">
									<div class="form-group label-floating is-empty">
										<label class="control-label">Email</label>
										<input class="form-control" placeholder="" type="email">
									</div>
									<div class="form-group label-floating is-empty">
										<label class="control-label">Password</label>
										<input class="form-control" placeholder="" type="password">
									</div>
			
									<div class="form-group label-floating is-select">
										<label class="control-label">Jenis Kelamin</label>
										<select class="selectpicker form-control">
											<option value="MA">Laki-laki</option>
											<option value="FE">Perempuan</option>
										</select>
									</div>
									<div class="form-group">
											<label class="control-label">Unggah Foto Profil</label>
											<input value="Unggah Foto Profil" type="file" name="foto-profil" class="file-upload__label" id="File">
									</div>
			
									<a href="#" class="btn btn-purple btn-lg full-width">Complete Registration!</a>
								</div>
							</div>
						</form>
					</div>
			
					<div class="tab-pane" id="profile" role="tabpanel" data-mh="log-tab">
						<div class="title h6">Login</div>
						<form class="content">
							<div class="row">
								<div class="col col-12 col-xl-12 col-lg-12 col-md-12 col-sm-12">
									<div class="form-group label-floating is-empty">
										<label class="control-label">Email</label>
										<input class="form-control" placeholder="" type="email">
									</div>
									<div class="form-group label-floating is-empty">
										<label class="control-label">Password</label>
										<input class="form-control" placeholder="" type="password">
									</div>
			
									<a href="#" class="btn btn-lg btn-primary full-width">Login</a>
			
			
									<p>Don’t you have an account? <a href="/">Register Now!</a> it’s really simple and you can start enjoing all the benefits!</p>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
			
			<!-- ... end Login-Registration Form  -->		</div>
	</div>
</div>


<!-- JS Scripts -->
<script src="{{ asset('/olympus-html/js/jquery-3.2.1.js')}}"></script>
<script src="{{ asset('/olympus-html/js/jquery.appear.js')}}"></script>
<script src="{{ asset('/olympus-html/js/jquery.mousewheel.js')}}"></script>
<script src="{{ asset('/olympus-html/js/perfect-scrollbar.js')}}"></script>
<script src="{{ asset('/olympus-html/js/jquery.matchHeight.js')}}"></script>
<script src="{{ asset('/olympus-html/js/svgxuse.js')}}"></script>
<script src="{{ asset('/olympus-html/js/imagesloaded.pkgd.js')}}"></script>
<script src="{{ asset('/olympus-html/js/Headroom.js')}}"></script>
<script src="{{ asset('/olympus-html/js/velocity.js')}}"></script>
<script src="{{ asset('/olympus-html/js/ScrollMagic.js')}}"></script>
<script src="{{ asset('/olympus-html/js/jquery.waypoints.js')}}"></script>
<script src="{{ asset('/olympus-html/js/jquery.countTo.js')}}"></script>
<script src="{{ asset('/olympus-html/js/popper.min.js')}}"></script>
<script src="{{ asset('/olympus-html/js/material.min.js')}}"></script>
<script src="{{ asset('/olympus-html/js/bootstrap-select.js')}}"></script>
<script src="{{ asset('/olympus-html/js/smooth-scroll.js')}}"></script>
<script src="{{ asset('/olympus-html/js/selectize.js')}}"></script>
<script src="{{ asset('/olympus-html/js/swiper.jquery.js')}}"></script>
<script src="{{ asset('/olympus-html/js/moment.js')}}"></script>
<script src="{{ asset('/olympus-html/js/daterangepicker.js')}}"></script>
<script src="{{ asset('/olympus-html/js/simplecalendar.js')}}"></script>
<script src="{{ asset('/olympus-html/js/fullcalendar.js')}}"></script>
<script src="{{ asset('/olympus-html/js/isotope.pkgd.js')}}"></script>
<script src="{{ asset('/olympus-html/js/ajax-pagination.js')}}"></script>
<script src="{{ asset('/olympus-html/js/Chart.js')}}"></script>
<script src="{{ asset('/olympus-html/js/chartjs-plugin-deferred.js')}}"></script>
<script src="{{ asset('/olympus-html/js/circle-progress.js')}}"></script>
<script src="{{ asset('/olympus-html/js/loader.js')}}"></script>
<script src="{{ asset('/olympus-html/js/run-chart.js')}}"></script>
<script src="{{ asset('/olympus-html/js/jquery.magnific-popup.js')}}"></script>
<script src="{{ asset('/olympus-html/js/jquery.gifplayer.js')}}"></script>
<script src="{{ asset('/olympus-html/js/mediaelement-and-player.js')}}"></script>
<script src="js/mediaelement-playlist-plugin.min.js')}}"></script>

<script src="{{ asset('/olympus-html/js/base-init.js')}}"></script>
<script defer src="{{ asset('/olympus-html/fonts/fontawesome-all.js')}}"></script>

<script src="{{ asset('/olympus-html/Bootstrap/dist/js/bootstrap.bundle.js')}}"></script>

</body>
</html>