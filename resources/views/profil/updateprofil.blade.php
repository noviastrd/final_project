@extends('main.master')

@section('profil')

<div class="container">
	<div class="row">
	<div class="col col-xl-9 order-xl-2 col-lg-9 order-lg-2 col-md-12 order-md-1 col-sm-12 col-12">
			<div class="ui-block">
				<div class="ui-block-title">
					<h6 class="title">Personal Information</h6>
				</div>
				<div class="ui-block-content">

					
					<!-- Personal Information Form  -->
					
					<form>
						<div class="row">
					
							<div class="col col-lg-6 col-md-6 col-sm-12 col-12">
								<div class="form-group label-floating">
									<label class="control-label">Nama Lengkap</label>
									<input class="form-control" placeholder="" type="text" value="James">
								<span class="material-input"></span></div>
					
								<div class="form-group label-floating">
									<label class="control-label">Your Email</label>
									<input class="form-control" placeholder="" type="email" value="jspiegel@yourmail.com">
								<span class="material-input"></span></div>
					
							</div>
							<div class="col col-lg-6 col-md-6 col-sm-12 col-12">
								<div class="form-group label-floating">
									<label class="control-label">Password</label>
									<input class="form-control" placeholder="" type="text" value="James">
								<span class="material-input"></span></div>
					
								<div class="form-group label-floating">
									<label class="control-label">Confirmasi Password</label>
									<input class="form-control" placeholder="" type="email" value="jspiegel@yourmail.com">
								<span class="material-input"></span></div>
					
							</div>
					
					
							
							
							<div class="col col-lg-6 col-md-6 col-sm-12 col-12">
								<button class="btn btn-primary btn-lg full-width">Save all Changes</button>
							</div>
					
						</div>
					</form>
					
					<!-- ... end Personal Information Form  -->
				</div>
			</div>
</div>
</div>
</div>
@endsection