<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Find Friends</title>

        <link rel="stylesheet" type="text/css" href="{{ asset('/olympus-html/Bootstrap/dist/css/bootstrap-reboot.css')}}">
        <link rel="stylesheet" type="text/css" href="{{ asset('/olympus-html/Bootstrap/dist/css/bootstrap.css')}}">
        <link rel="stylesheet" type="text/css" href="{{ asset('/olympus-html/Bootstrap/dist/css/bootstrap-grid.css')}}">

        <!-- Main Styles CSS -->
        <link rel="stylesheet" type="text/css" href="{{ asset('/olympus-html/css/main.min.css')}}">
        <link rel="stylesheet" type="text/css" href="{{ asset('/olympus-html/css/fonts.min.css')}}">


        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;600&display=swap" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>


    <body class="landing-page">


        <div class="content-bg-wrap"></div>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}" class="btn btn-md btn-border c-white">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}" class="btn btn-md btn-border c-white">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="col col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                        <div class="landing-content">
                            <h1>Sealamat datang di Web FindFriends</h1>
                            <p>Pada web kami kalian dapat mencari sebanyak-banyaknya teman
                            </p>
                            
                        </div>
                    </div>
                
            </div>
        </div>
    </body>
</html>
