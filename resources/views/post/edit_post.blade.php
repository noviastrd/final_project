@extends('main.master')

@section('content')
<main class="col col-xl-8 order-xl-2 col-lg-12 order-lg-1 col-md-12 col-sm-12 col-12">
<div class="ui-block">
				
				<!-- News Feed Form  -->
				
				<div class="news-feed-form">
					<!-- Nav tabs -->
					<ul class="nav nav-tabs" role="tablist">
						<li class="nav-item">
							<a class="nav-link active inline-items" data-toggle="tab" href="#home-1" role="tab" aria-expanded="true">
				
								<svg class="olymp-status-icon"><use xlink:href="{{ asset('/olympus-html/svg-icons/sprites/icons.svg#olymp-status-icon')}}"></use></svg>
				
								<span>Status</span>
							</a>
						</li>
						
					</ul>
					              
					              <!-- form start -->
					        <form role="form" action="/post/{{$postingan->id}}" method="POST">
					        @csrf
					        @method('PUT')
					            <div class="card-body">
					                
					                <div class="form-group">
				                    <label for="isi_postingan">Isi</label>
				                    <input type="longtext" class="form-control" id="isi_postingan" name="isi_postingan" value="{{ old('isi_postingan', $postingan->isi_postingan) }}" placeholder="">
				                    @error('isi_postingan')
				                        <div class="alert alert-danger">{{ $message }}</div>
				                    @enderror
				                </div>

					                <div class="form-group">
				                      <label class="control-label">Unggah Foto</label>
				                      <input type="file" value="Unggah Foto" type="file" name="image" class="file-upload__label" id="File">
				                  </div>

					                  
					            </div>
					            <!-- /.card-body -->

					            <div class="card-footer">
					                  <button type="submit" class="btn btn-primary">Submit</button>
					            </div>
					            </form>
					</div>


				

				</div>
	</div>
</main>


@endsection