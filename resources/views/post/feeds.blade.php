@extends('main.master')

@section('content')



<div id="newsfeed-items-grid">
	


				<div class="ui-block">

					
					<article class="hentry post">
					
						<div class="post__author author vcard inline-items">
							<img src="{{ asset('/olympus-html/img/avatar10-sm.jpg')}}" alt="author">
					
							<div class="author-date">
								<a class="h6 post__author-name fn" href="#">Elaine Dreyfuss</a>
								<div class="post__date">
									<time class="published" datetime="2004-07-24T18:18">
										9 hours ago
									</time>
								</div>
							</div>
					
							<div class="more"><svg class="olymp-three-dots-icon"><use xlink:href="svg-icons/sprites/icons.svg#olymp-three-dots-icon"></use></svg>
								<ul class="more-dropdown">
									<li>
										<a href="#">Edit Post</a>
									</li>
									<li>
										<a href="#">Delete Post</a>
									</li>
									<li>
										<a href="#">Turn Off Notifications</a>
									</li>
									<li>
										<a href="#">Select as Featured</a>
									</li>
								</ul>
							</div>
					
						</div>
					
                  	
                      <p>
                      	
                      </p>
                       
                      
                      
               
                   
                  	  					
						<div class="post-additional-info inline-items">
					
							<a href="#" class="post-add-icon inline-items">
								<svg class="olymp-heart-icon"><use xlink:href="{{ asset('/olympus-html/svg-icons/sprites/icons.svg#olymp-heart-icon')}}"></use></svg>
								<span>24</span>
							</a>
					
							<ul class="friends-harmonic">
								<li>
									<a href="#">
										<img src="{{ asset('/olympus-html/img/friend-harmonic7.jpg')}}" alt="friend">
									</a>
								</li>
								<li>
									<a href="#">
										<img src="{{ asset('/olympus-html/img/friend-harmonic8.jpg')}}" alt="friend">
									</a>
								</li>
								<li>
									<a href="#">
										<img src="{{ asset('/olympus-html/img/friend-harmonic9.jpg')}}" alt="friend">
									</a>
								</li>
								<li>
									<a href="#">
										<img src="img/friend-harmonic10.jpg" alt="friend">
									</a>
								</li>
								<li>
									<a href="#">
										<img src="{{ asset('/olympus-html/img/friend-harmonic11.jpg')}}" alt="friend">
									</a>
								</li>
							</ul>
					
							<div class="names-people-likes">
								<a href="#">You</a>, <a href="#">Elaine</a> and
								<br>22 more liked this
							</div>
					
					
							<div class="comments-shared">
								<a href="/komentar" class="post-add-icon inline-items">
									<svg class="olymp-speech-balloon-icon"><use xlink:href="{{ asset('/olympus-html/svg-icons/sprites/icons.svg#olymp-speech-balloon-icon')}}"></use></svg>
									<span>17</span>
								</a>
			
								
							</div>
					
					
						</div>
					
						<div class="control-block-button post-control-button">
					
							<a href="#" class="btn btn-control">
								<svg class="olymp-like-post-icon"><use xlink:href="{{ asset('/olympus-html/svg-icons/sprites/icons.svg#olymp-like-post-icon')}}"></use></svg>
							</a>
					
							<a href="#" class="btn btn-control">
								<svg class="olymp-comments-post-icon"><use xlink:href="{{ asset('/olympus-html/svg-icons/sprites/icons.svg#olymp-comments-post-icon')}}"></use></svg>
							</a>
					
							
					
						</div>
					
					</article>
					
					<!-- Comments -->
					

					
					<!-- ... end Comments -->

					
					<!-- Comment Form  -->
			


			</div>




<div class="card">
             <div class="card-header">
                <h3 class="card-title">Postingan Table</h3>
              </div>
              
              <!-- /.card-header -->
              <div class="card-body">
              	@if(session('success'))
              		<div class="alert alert-success">
              			{{ session('success')}}
              		</div>
              	@endif
              	<a class="btn btn-primary" href="{{route('post.create')}}">Buat Postingan baru</a>
                <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th style="width: 10px">#</th>
                      <th>User</th>
                      <th>Isi</th>
                      <th>Waktu</th>
                      <th style="width: 200px">Action </th>
                    </tr>
                  </thead>
                  <tbody>
                  	@forelse($postingan as $key => $postingan)
                  	<tr>
                      <td> {{ $key +1 }}</td>
                      <td> {{ $postingan->name }}</td>

                      <td> {{ $postingan->isi_postingan }}
                      </td>
                      <td> {{ $postingan->created_at }}</td>
                      <td style="display: flex;"> 
                      	<div class="post-control-button">
					
							<div class="post-additional-info inline-items">
					
							<a href="#" class="post-add-icon inline-items">
								<svg class="olymp-heart-icon"><use xlink:href="{{ asset('/olympus-html/svg-icons/sprites/icons.svg#olymp-heart-icon')}}"></use></svg>
								<span>24</span>
							</a>
		
								<a href="post/{{$postingan->id}}" class="post-add-icon inline-items">
									<svg class="olymp-speech-balloon-icon"><use xlink:href="{{ asset('/olympus-html/svg-icons/sprites/icons.svg#olymp-speech-balloon-icon')}}"></use></svg>
									<span>17</span>
								</a>
								@if ($postingan->user_id == Auth::id())
								<a href="/post/{{$postingan->id}}/edit" class="btn btn-primary btn-sm">edit</a>
								@endif

								@if ($postingan->user_id == Auth::id())
								<form action="post/{{$postingan->id}}" method="POST">
                      		@csrf
                      		@method('DELETE')
                      		<input type="submit" value="delete" class="btn btn-danger btn-sm">
                      		@endif
                      	</form>
	
					
					
						</div>
					
							
					
						</div>
                      </td>
                    </tr>
                    @empty
                    <tr>
                    	<td colspan="4" align="center">No Data</td>
                    </tr>
                  	@endforelse                    
                  </tbody>
                </table>
              </div>
</div>





@endsection