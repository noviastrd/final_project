@extends('main.master')

@section('content')

<div id="newsfeed-items-grid">

				<div class="ui-block">

					
					<article class="hentry post">
					
						<div class="post__author author vcard inline-items">
							<img src="{{ asset('/olympus-html/img/avatar10-sm.jpg')}}" alt="author">
					
							<div class="author-date">
								<a class="h6 post__author-name fn" href="#">Elaine Dreyfuss</a>
								<div class="post__date">
									<time class="published" datetime="2004-07-24T18:18">
										9 hours ago
									</time>
								</div>
							</div>
					
							<div class="more"><svg class="olymp-three-dots-icon"><use xlink:href="svg-icons/sprites/icons.svg#olymp-three-dots-icon"></use></svg>
								<ul class="more-dropdown">
									<li>
										<a href="#">Edit Post</a>
									</li>
									<li>
										<a href="#">Delete Post</a>
									</li>
									<li>
										<a href="#">Turn Off Notifications</a>
									</li>
									<li>
										<a href="#">Select as Featured</a>
									</li>
								</ul>
							</div>
					
						</div>
					
						<p>
							{{$postingan->isi_postingan}} 
						</p>
					
						<div class="post-additional-info inline-items">
					
							<a href="#" class="post-add-icon inline-items">
								<svg class="olymp-heart-icon"><use xlink:href="{{ asset('/olympus-html/svg-icons/sprites/icons.svg#olymp-heart-icon')}}"></use></svg>
								<span>24</span>
							</a>
					
							<ul class="friends-harmonic">
								<li>
									<a href="#">
										<img src="{{ asset('/olympus-html/img/friend-harmonic7.jpg')}}" alt="friend">
									</a>
								</li>
								<li>
									<a href="#">
										<img src="{{ asset('/olympus-html/img/friend-harmonic8.jpg')}}" alt="friend">
									</a>
								</li>
								<li>
									<a href="#">
										<img src="{{ asset('/olympus-html/img/friend-harmonic9.jpg')}}" alt="friend">
									</a>
								</li>
								<li>
									<a href="#">
										<img src="img/friend-harmonic10.jpg" alt="friend">
									</a>
								</li>
								<li>
									<a href="#">
										<img src="{{ asset('/olympus-html/img/friend-harmonic11.jpg')}}" alt="friend">
									</a>
								</li>
							</ul>
					
							<div class="names-people-likes">
								<a href="#">You</a>, <a href="#">Elaine</a> and
								<br>22 more liked this
							</div>
					
					
							<div class="comments-shared">
								<a href="#" class="post-add-icon inline-items">
									<svg class="olymp-speech-balloon-icon"><use xlink:href="{{ asset('/olympus-html/svg-icons/sprites/icons.svg#olymp-speech-balloon-icon')}}"></use></svg>
									<span>17</span>
								</a>
					
								
							</div>
					
					
						</div>
					
						<div class="control-block-button post-control-button">
					
							<a href="#" class="btn btn-control">
								<svg class="olymp-like-post-icon"><use xlink:href="{{ asset('/olympus-html/svg-icons/sprites/icons.svg#olymp-like-post-icon')}}"></use></svg>
							</a>
					
							<a href="#" class="btn btn-control">
								<svg class="olymp-comments-post-icon"><use xlink:href="{{ asset('/olympus-html/svg-icons/sprites/icons.svg#olymp-comments-post-icon')}}"></use></svg>
							</a>
					
							
					
						</div>
					
					</article>
					
					<!-- Comments -->
					
					<ul class="comments-list">
						<li class="comment-item">
							<div class="post__author author vcard inline-items">
								<img src="img/author-page.jpg" alt="author">
					
								<div class="author-date">
									<a class="h6 post__author-name fn" href="{{ asset('/olympus-html/02-ProfilePage.html')}}">James Spiegel</a>
									<div class="post__date">
										<time class="published" datetime="2004-07-24T18:18">
											38 mins ago
										</time>
									</div>
								</div>
					
								<a href="#" class="more"><svg class="olymp-three-dots-icon"><use xlink:href="{{ asset('/olympus-html/svg-icons/sprites/icons.svg#olymp-three-dots-icon')}}"></use></svg></a>
					
							</div>
					
							<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium der doloremque laudantium.</p>
					
							<a href="#" class="post-add-icon inline-items">
								<svg class="olymp-heart-icon"><use xlink:href="{{ asset('/olympus-html/svg-icons/sprites/icons.svg#olymp-heart-icon')}}"></use></svg>
								<span>3</span>
							</a>
													</li>
						<li class="comment-item">
							<div class="post__author author vcard inline-items">
								<img src="{{ asset('/olympus-html/img/avatar1-sm.jpg')}}" alt="author">
					
								<div class="author-date">
									<a class="h6 post__author-name fn" href="#">Mathilda Brinker</a>
									<div class="post__date">
										<time class="published" datetime="2004-07-24T18:18">
											1 hour ago
										</time>
									</div>
								</div>
					
								<a href="#" class="more"><svg class="olymp-three-dots-icon"><use xlink:href="{{ asset('/olympus-html/svg-icons/sprites/icons.svg#olymp-three-dots-icon')}}"></use></svg></a>
					
							</div>
					
							<p>Ratione voluptatem sequi en lod nesciunt. Neque porro quisquam est, quinder dolorem ipsum
								quia dolor sit amet, consectetur adipisci velit en lorem ipsum duis aute irure dolor in reprehenderit in voluptate velit esse cillum.
							</p>
					
							<a href="#" class="post-add-icon inline-items">
								<svg class="olymp-heart-icon"><use xlink:href="{{ asset('/olympus-html/svg-icons/sprites/icons.svg#olymp-heart-icon')}}"></use></svg>
								<span>8</span>
							</a>
							
						</li>
					</ul>
					
					<!-- ... end Comments -->

					<a href="#" class="more-comments">View more comments <span>+</span></a>

					
					<!-- Comment Form  -->
					
					<form class="comment-form inline-items">
					
						<div class="post__author author vcard inline-items">
							<img src="{{ asset('/olympus-html/img/author-page.jpg')}}" alt="author">
					
							<div class="form-group with-icon-right ">
								<textarea class="form-control" placeholder=""></textarea>
			
							</div>
						</div>
					
						<button class="btn btn-md-2 btn-primary">Post Comment</button>
					
						<button class="btn btn-md-2 btn-border-think c-grey btn-transparent custom-color">Cancel</button>
					
					</form>
					
					<!-- ... end Comment Form  -->
				</div>



			</div>

@endsection